# cookiecutter-python-lib

generates project code for a simple python lib

## Installation

    git clone http://bitbucket.org/mswinson-lib/cookiecutter-python-lib


## Usage

    cookiecutter ./cookiecutter-python-lib


## Contributing

1. Fork it ( https://bitbucket.org/mswinson-lib/cookiecutter-python-lib/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
