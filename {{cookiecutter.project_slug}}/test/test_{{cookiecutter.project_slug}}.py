from nose.tools import assert_equals
import {{cookiecutter.project_slug}}

class Test{{cookiecutter.project_slug.lower().capitalize()}}:
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_version(self):
        assert_equals({{cookiecutter.project_slug}}.VERSION,"0.1.0")

