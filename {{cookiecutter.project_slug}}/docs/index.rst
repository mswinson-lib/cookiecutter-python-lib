Welcome to {{cookiecutter.project_name}}'s documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   readme
   modules
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
