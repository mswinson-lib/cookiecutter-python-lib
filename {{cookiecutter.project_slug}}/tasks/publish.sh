#!/usr/bin/env bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
shopt -s nullglob
for f in $DIR/publish/*.sh
do
  $f $@
done

