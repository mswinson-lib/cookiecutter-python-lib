#!/usr/bin/env bash
TARGET_DIR=$1
ARTIFACT_DIR=$2
REPORT_DIR=${ARTIFACT_DIR}/reports

rm -rf .coverage
mkdir -p ${TARGET_DIR}/test-reports
nosetests
mkdir -p ${REPORT_DIR}/test
cp -R ${TARGET_DIR}/test-reports/* ${REPORT_DIR}/test
rm -rf .coverage

