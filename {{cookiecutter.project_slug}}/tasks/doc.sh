#!/usr/bin/env bash
ARTIFACT_DIR=$1
PROJECT_NAME=$2
API_DIR=${ARTIFACT_DIR}/api

mkdir -p ${API_DIR}
rm -f docs/${PROJECT_NAME}.rst
rm -f docs/modules.rst
sphinx-apidoc -o docs/ lib/${PROJECT_NAME}
make -C docs clean
make -C docs html
mkdir -p ${API_DIR}
cp -R docs/_build/html/* ${API_DIR}
