# Change Log

**13/07/2018**

**0.2.0**

improvements  

    adds docker compose stack  
    adds bitbucket pipelines integration
    adds readthedocs integration to build system  

changes      

    move location of python module ./ -> ./lib
    refactor Make build system

fixes  

    fixes default cookiecutter settings
    fixes python style issues  


**v0.1.0**

  first version  

